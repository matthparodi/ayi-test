import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { map } from 'rxjs/operators';
@Injectable({
  providedIn: 'root',
})
export class SpotifyService {
  constructor(private http: HttpClient) {

  }


  getQuery(query: string) {
    const url = `https://api.spotify.com/v1/${query}`;

    const headers = new HttpHeaders({
      Authorization: 'Bearer BQCMmjfKry1ymFBAaDxvR3I6pzGIvx4ynOX6fKJO8sXERQy_EadiITA4egLqo0IGxLdTtqUKTZKUsuwhftMD--IagFSD3srqlt6Y7Z0ol3H5wlNYsWgyThALs-VVa-GwqnCsSo6Y23c0cXHeN4c'
    });

    return this.http.get(url, {headers});
  }

  getNewReleases() {
    return this.getQuery('browse/new-releases').pipe(
      map(data => data['albums'].items)
    );
  }
}
