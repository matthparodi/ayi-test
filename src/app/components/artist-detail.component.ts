import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-artist-detail',
  templateUrl: './artist-detail.component.html'
})
export class ArtistDetailComponent implements OnInit {
  title = 'My Spotify App';

  @Input() artist;
  ngOnInit() {

  }

}
