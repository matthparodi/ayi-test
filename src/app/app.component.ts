import { Component, OnInit } from '@angular/core';
import { SpotifyService } from './services/spotify.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'My Spotify App';
  selectedArtist = {};
  artists = [];

  constructor(private spotifyService: SpotifyService) {

  }

  ngOnInit() {
    this.spotifyService.getNewReleases().subscribe((resp)=>{
      console.log(resp);
    });
  }

  changeTitle(newTitle: string) {
    this.title = newTitle;
  }

  selectArtist(artist) {

  }

}
